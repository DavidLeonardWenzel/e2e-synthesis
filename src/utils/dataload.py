import numpy as np
import os
import pickle
import torch
from torch.nn.utils.rnn import pad_packed_sequence
from torch.utils.data import Dataset
from utils.misc import file_exist, collect_folder_contents, read
from utils.PreprocessingForTTS.ProcessAudio import AudioPreprocessor as audioprep
import soundfile as sf
from utils.misc import read


class CustomDataset(Dataset):
    def __init__(self,
                 path="src/data/"):
                 # for_generator=True,
                 # for_discriminator=False):
        self.path = path
        self.fidelity = None
        self.filenames = self.get_filenames()
        self.wav_tensors = []
        self.wav_orig_length = []  # already at 200hz
        self.wav_cut_value = []  # already at 200hz

        self.phone_tensors = []
        self.pad_start_values = []

        self.phone_duration_tensors = []

        for filename in self.filenames:
            wave_sample = torch.load(filename[0])
            phone_sample = torch.load(filename[1])
            phone_duration = torch.load(filename[2])

            # cut phone pad value from phone
            # cut cut value and orig timestep value from wav
            self.wav_orig_length.append(wave_sample[0])
            self.wav_cut_value.append(wave_sample[1])
            self.wav_tensors.append(wave_sample[2:])

            self.pad_start_values.append(phone_sample[0])
            self.phone_tensors.append(phone_sample[1:])

            self.phone_duration_tensors.append(phone_duration)
            # check len of wave sample after this operation

        print("loaded {} files".format(len(self.filenames)))

    def __getitem__(self, index):

        return self.wav_tensors[index], self.wav_orig_length[index], self.wav_cut_value[index], self.phone_tensors[index], self.pad_start_values[index], self.phone_duration_tensors[index]

    def __len__(self):
        return len(self.filenames)

    def get_filenames(self):
        file_names = read(os.path.join(
            self.path, "file_name_list.txt")).split("\n")
        self.fidelity = int(file_names[-1])
        file_names = file_names[:-1]  # cut out fidelity
        file_triplets = []
        for file in file_names:
            file_triplets.append(file.split("|"))
        return file_triplets


if __name__ == "__main__":
    path = os.join("data", "eng", "4kHz")
    loader = CustomDataset(path)

import os
import numpy as np
import torch
import torch.nn.functional as F
import torch.nn as nn
import soundfile
import matplotlib.pyplot as plt
import librosa.display as lbd
from torchaudio.transforms import MuLawDecoding


def log_step(global_step, predicted_token_lengths, actual_sequence_length, waves, upsampled_aligned_phones, ToMelSpectrogramLayer):
    print("log step")
    # create a directory with step name in the logdir
    print_up_till_batch = 3
    os.makedirs("logdir/samples/step" +
                str(global_step), exist_ok=True)
    file_loc = "logdir/samples/step" + str(global_step)+"/"
    for batch_id in range(print_up_till_batch):
        # sentence:
        content = "total: " + \
            str(torch.sum(predicted_token_lengths, dim=1)) + "\n"
        content += "truth: " + \
            str(actual_sequence_length) + "\n"
        content += str(predicted_token_lengths[batch_id])
        write(os.path.join(file_loc, "durations_{}.txt".format(batch_id)),
              content)
        # wav:
        upsampled_aligned_phones = MuLawDecoding()(upsampled_aligned_phones)
        waves = MuLawDecoding()(waves)
        read_to_file(upsampled_aligned_phones[batch_id].squeeze(),
                     os.path.join(file_loc, "predicted_wav_{}.wav".format(batch_id)))
        read_to_file(waves[batch_id].squeeze(),
                     os.path.join(file_loc, "real_wav_{}.wav".format(batch_id)))

        visualize_output(
            upsampled_aligned_phones.squeeze()[
                batch_id].to("cpu"),
            os.path.join(file_loc, "predicted_stft_{}.png".format(batch_id)), ToMelSpectrogramLayer, apply_mu_decode=False)
        visualize_output(
            waves.squeeze(1)[batch_id].to("cpu"),
            os.path.join(file_loc, "target_stft_{}.png".format(batch_id)), ToMelSpectrogramLayer, apply_mu_decode=False)


class RandomCrop(nn.Module):
    def __init__(self,
                 size,
                 jitter_steps):
        super(RandomCrop, self).__init__()
        self.size = size
        self.jitter_steps = jitter_steps

    def forward(self, x):
        # initialize an empty copy to fill cropped waves into:
        cropped = torch.zeros_like(x)
        # pad 60 steps on each side, resulting in [N, 48120]
        x = F.pad(x, [self.jitter_steps, self.jitter_steps])
        # cut beginning from index 0-120 randomly, up to size
        # if crop = 0, we have 60 jittervalues at the start and cut the last 60 true values in the end
        # if crop = 60, x stays the same:
        #       we have cut the start jittervalues, and go till the end of the normal wave (60:48060),
        #       then cut the end jitter values (48060-48120 won't be included)
        # if crop = 120, we cut the first 60 jittervalues + first 60 true values, then include 60 end-jitter values

        for i, wave in enumerate(x):
            crop = torch.randint(0, self.jitter_steps*2, (1,))
            steps_till_size = self.size+crop
            cropped[i] = wave[crop:steps_till_size]
        return cropped


def read_to_file(wav, file_location):
    """
    :param file_location: The path and name of the file it should be saved to
    """
    soundfile.write(file=file_location,
                    data=wav.cpu().numpy(), samplerate=24000)


def ortho(model, strength=1e-4):
    ##########################
    # Orthogonal Regularization
    ##########################
    # updates weights of generator to keep them orthogonal
    # source:
    # https://github.com/ajbrock/BigGAN-PyTorch/blob/b70f16c4a879b2d5d5d7bcb73794424aef5eec1f/utils.py
    # usage:
    # #https://github.com/ajbrock/BigGAN-PyTorch/blob/b70f16c4a879b2d5d5d7bcb73794424aef5eec1f/train_fns.py#L51

    # Apply modified ortho reg to a model
    # This function is an optimized version that directly computes the gradient,
    # instead of computing and then differentiating the loss.
    with torch.no_grad():
        for name, param in model.named_parameters():
            # Only apply this to parameters with at least 2 axes, and not in the blacklist
            if len(param.shape) < 2 or "conv" not in name:
                continue
            # print(name)
            w = param.view(param.shape[0], -1)
            grad = (2 * torch.mm(torch.mm(w, w.t())
                                 * (1. - torch.eye(w.shape[0], device=w.device)), w))
            param.grad.data += strength * grad.view(param.shape)


def mu_law_encoding(x, quantization_channels: int):
    """taken from torch.F and updated to be differentiable"""
    r"""Encode signal based on mu-law companding.  For more info see the
        `Wikipedia Entry <https://en.wikipedia.org/wiki/%CE%9C-law_algorithm>`_

        This algorithm assumes the signal has been scaled to between -1 and 1 and
        returns a signal encoded with values from 0 to quantization_channels - 1.

        Args:
            x (Tensor): Input tensor
            quantization_channels (int): Number of channels

        Returns:
            Tensor: Input after mu-law encoding
        """
    mu = quantization_channels - 1.0
    # if not x.is_floating_point():
    #    x = x.to(torch.float)
    mu = torch.tensor(mu, dtype=x.dtype)
    x_mu = torch.sign(x) * torch.log1p(mu * torch.abs(x)) / torch.log1p(mu)
    x_mu = ((x_mu + 1) / 2 * mu + 0.5)  # .to(torch.int64)
    return x_mu


def to_timestep(batch_seconds, fidelity):
    """for a list of duratoins in seconds, return a list of timesteps in the given samplerate by multiplying them 

    Args:
        batch_seconds (list of floats): seconds
        fidelity (int): the target fidelity

    Returns:
        list of floats: a list of timesteps
    """
    return [seconds * fidelity for seconds in batch_seconds]


# Convenience function to count the number of parameters in a module
def count_parameters(module):
    """counts and prints parameters given a module, no return value

    Args:
        module (a torch module): torch module
    """
    print('Number of parameters: {}'.format(
        sum([p.data.nelement() for p in module.parameters()])))


def pad_phones_and_durs(phone_tensor, phone_durations, pad_value):
    """pads a 1D tensor (usually phone IDs or durations) with 0s until they reach the pad value in length

    Args:
        phone_tensor (torch tensor): phone or character (letter)IDs
        phone_durations (torch tensor): phone or character durations
        pad_value (int): till what length to pad

    Returns:
        tripple-tuple (tensor, tensor, int): the padded input token tensor, the padded input durations tensor, the original len before padding
    """
    amount_of_phones = len(phone_tensor)

    pad_amount = pad_value - amount_of_phones
    text_padded = F.pad(phone_tensor, [0, pad_amount], "constant")
    dur_padded = F.pad(phone_durations, [
                       0, pad_amount], "constant", value=0.0000)

    return text_padded, dur_padded, amount_of_phones


def read(path):
    """reads and returns a file from a given path"""
    with open(path, 'r', encoding="utf-8") as f:
        return f.read()


def write(path, content):
    """writes a file to the given path, containing the given content. overwrites in case file exists"""
    my_f = open(path, "w", encoding="utf-8")
    my_f.write(content)
    my_f.close()


def file_exist(filepath):
    return os.path.isfile(filepath)


def collect_folder_contents(directory):
    return os.listdir(directory)


def visualize_output(wav, file_loc, ToMelSpectrogramLayer, apply_mu_decode):
    spec = ToMelSpectrogramLayer()(wav, apply_mu_decode=apply_mu_decode, jitter=False)

    fig, ax = plt.subplots(nrows=2, ncols=1, figsize=(4, 3))
    ax[0].plot(wav.cpu().numpy())
    lbd.specshow(spec.numpy(), ax=ax[1], sr=24000, cmap='GnBu', y_axis='mel', x_axis='time',
                 hop_length=1024)
    # ax[0].set_title(self.text2phone.get_phone_string(text))
    ax[0].yaxis.set_visible(False)
    ax[1].yaxis.set_visible(False)
    plt.subplots_adjust(left=0.05, bottom=0.1,
                        right=0.95, top=.9, wspace=0.0, hspace=0.0)
    plt.show()
    plt.savefig(file_loc)
    # print("plotted!")


def save_checkpoint(checkpoints, generator, discriminator,
                    g_optimizer, d_optimizer, g_scheduler, d_scheduler, step, ema=None):
    checkpoint_path = os.path.join(
        checkpoints, "model.ckpt-{}.pt".format(step))

    torch.save({"generator": generator.state_dict(),
                "discriminator": discriminator.state_dict(),
                "g_optimizer": g_optimizer.state_dict(),
                "d_optimizer": d_optimizer.state_dict(),
                "g_scheduler": g_scheduler.state_dict(),
                "d_scheduler": d_scheduler.state_dict(),
                "global_step": step,
                }, checkpoint_path)

    print("Saved checkpoint: {}".format(checkpoint_path))

    with open(os.path.join(checkpoints, 'checkpoint'), 'w') as f:
        f.write("model.ckpt-{}.pt".format(step))


def load_checkpoint(checkpoint_path, use_cuda):
    if use_cuda:
        checkpoint = torch.load(checkpoint_path)
    checkpoint = torch.load(
        checkpoint_path, map_location=lambda storage, loc: storage)
    return checkpoint


def attempt_to_restore(generator, discriminator, g_optimizer,
                       d_optimizer, g_scheduler, d_scheduler, checkpoint_name, use_cuda):
    checkpoint_list = os.path.join('checkpoints', checkpoint_name)

    # if os.path.exists(checkpoint_list):
    # checkpoint_filename = open(checkpoint_list).readline().strip()
    checkpoint_path = os.path.join(
        "checkpoints", "{}".format(checkpoint_name))
    print("Restore from {}".format(checkpoint_path))
    checkpoint = load_checkpoint(checkpoint_path, use_cuda)
    generator.load_state_dict(checkpoint["generator"])
    g_optimizer.load_state_dict(checkpoint["g_optimizer"])
    g_scheduler.load_state_dict(checkpoint["g_scheduler"])
    discriminator.load_state_dict(checkpoint["discriminator"])
    d_optimizer.load_state_dict(checkpoint["d_optimizer"])
    d_scheduler.load_state_dict(checkpoint["d_scheduler"])

    global_step = checkpoint["global_step"]

    # print("no checkpoints found")
    # global_step = 0
    return global_step

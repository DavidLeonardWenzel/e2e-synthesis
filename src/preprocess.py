import os
import argparse
import numpy as np
from tqdm import tqdm
import os
import utils.PreprocessingForTTS.ProcessText as textprep
import utils.PreprocessingForTTS.ProcessAudio as audioprep
import json
from utils.misc import read, to_timestep, pad_phones_and_durs
import random

import torch
import soundfile as sf
from torchaudio.transforms import MuLawDecoding


predetermined_phones_with_ground_truth = True


def find_files(path, pattern):
    files = []
    for file in os.listdir(path):
        if file.endswith(pattern):
            files.append(os.path.join(path, file))
    return files


def process(file_names, args, train_dir, audio_prep_tool, text_prep_tool):
    # should pickle three tensors to three folders for training:
    # 1st:
    # - phonemized trasncription, 0-padded to 400 characters
        # - additional info: pad start value (first value of phones per utterance)
    # 2nd:
    # - ground truth duration in timsteps at 200hz
    # 3rd:
    # - wav (cut to 2s), sampled to wanted fidelity, 24khz
    # - wav cut value at 200hz (first value of wav per utterance)

    names = []
    # 2 seconds of audio is our minimum, the sample rate is the timesteps per second, thus 2 seconds of timesteps is:
    minimum_audio_length = audio_prep_tool.final_sr * 2
    downsample_rate = audio_prep_tool.final_sr / 200
    pad_value = 400
    print("processing {} data samples".format(len(file_names)))
    # alignments:
    phone_alignments = read_alignment_file("../alignments.json")
    for file_name in tqdm(file_names):
        # load audio:
        wave_sample, _dataset_sr = sf.read(file_name[0])
        wave_tensor = audio_prep_tool.audio_to_wave_tensor(
            wave_sample, normalize=True, mulaw=True)
        # check len of wave sample and wave tensor
        timesteps = len(wave_tensor)
        if timesteps > minimum_audio_length:
            # get a random 2 sec window:
            # cut my wave into pieces!
            # this is my train window!
            max_audio_start = timesteps - minimum_audio_length
            audio_start = random.randint(0, max_audio_start)
            two_second_wave = torch.LongTensor(
                wave_tensor[audio_start:(audio_start+minimum_audio_length)]
            )
            audio_start_at_200 = audio_start / downsample_rate
            timesteps_at_200 = timesteps / downsample_rate
            wave_tensor = torch.cat((torch.Tensor([timesteps_at_200]),
                                     torch.Tensor([audio_start_at_200]),
                                     two_second_wave))

            # find respective text to process, then save both tensors
            if predetermined_phones_with_ground_truth == True:
                phones, phone_durations = get_phones_and_durations(
                    phone_alignments, file_name[0])
                phones.append(1)  # EOS token!
                # get the durations in timesteps in 200Hz:
                phone_durations = to_timestep(phone_durations, 200)
                phones = torch.tensor(phones)
                phone_durations = torch.tensor(phone_durations)
            else:
                # use text prep string to tensor:
                text = read(file_name[1])
                phones = text_prep_tool.string_to_tensor(text)
                phone_durations = torch.zeros_like(phones)

            padded_phones, padded_durs, start_pad_value = pad_phones_and_durs(
                phones, phone_durations, pad_value)
            padded_phones = torch.cat(
                (torch.Tensor([start_pad_value]).long(), torch.tensor(padded_phones, dtype=torch.long)))

            audio_file_name = os.path.basename(
                file_name[0]).replace('.wav', '.pt')
            text_file_name = os.path.basename(
                file_name[1]).replace('.txt', '.pt')

            full_audio_path = os.path.join(
                train_dir, "audio", audio_file_name)
            full_text_path = os.path.join(
                train_dir, "txt", text_file_name)
            full_dur_path = os.path.join(
                train_dir, "duration", text_file_name
            )

            names.append([full_audio_path, full_text_path, full_dur_path])

            torch.save(wave_tensor, full_audio_path)
            torch.save(padded_phones, full_text_path)
            torch.save(padded_durs, full_dur_path)

    return names


def get_phones_and_durations(phone_alignments, file_name):
    base_path = "/mount/resources/speech/corpora/LJSpeech/16kHz/wav/"
    return phone_alignments[base_path + file_name.split("/")[-1]]


def read_alignment_file(path):
    with open(path) as json_file:
        data = json.load(json_file)
    return data


def preprocess(args):
    """sets up folder struct"""
    train_dir = os.path.join(args.output_dir, 'train')
    test_dir = os.path.join(args.output_dir, 'test')
    # os.makedirs(args.output, exist_ok=True)
    os.makedirs(train_dir, exist_ok=True)
    os.makedirs(test_dir, exist_ok=True)
    os.makedirs(os.path.join(train_dir, "audio"), exist_ok=True)
    os.makedirs(os.path.join(train_dir, "txt"), exist_ok=True)
    os.makedirs(os.path.join(train_dir, "duration"), exist_ok=True)
    wav_files = find_files(args.wav_dir, ".wav")
    txt_files = find_files(args.txt_dir, ".txt")
    file_names = list(zip(sorted(wav_files), sorted(txt_files)))

    # load a single wav to get datasets target_sr
    _wave, dataset_sr = sf.read(file_names[0][0])
    audio_prep_tool = audioprep.AudioPreprocessor(
        input_sr=dataset_sr, output_sr=args.sr)
    text_prep_tool = textprep.TextFrontend(language=args.language,
                                           use_panphon_vectors=False,
                                           use_explicit_eos=True,
                                           path_to_panphon_table="utils/PreprocessingForTTS/ipa_vector_lookup.csv")

    file_name_list = process(file_names, args, train_dir,
                             audio_prep_tool, text_prep_tool)
    write_file_name_list(file_name_list, args.output_dir, args.sr)
    # load a single wav and phone tensor, save it as .wav and .txt:
    sanity_check(args, audio_prep_tool, text_prep_tool)


def sanity_check(args, audio_prep_tool, text_prep_tool):
    file_names = read(os.path.join(
        args.output_dir, "file_name_list.txt")).split("\n")[:-1]  # final line is only fidelity
    file_pairs = []
    for file in file_names:
        file_pairs.append(file.split("|"))
    wav_sample = file_pairs[-1][0]  # take last line,
    phone_sample = file_pairs[-1][1]
    duration_sample = file_pairs[-1][2]

    phone_durations = torch.load(duration_sample)
    total_length = round(float(sum(phone_durations)), 2)
    # first value is the length of the total audio!
    wav = torch.load(wav_sample)
    time_steps_in_200 = wav[0]
    audio_start_at_200 = wav[1]
    wav = wav[2:]
    mulawdecoder = MuLawDecoding()
    true_wav_duration = round(len(wav) / 24000, 2)

    assert(true_wav_duration == 2.0)
    assert(torch.round(time_steps_in_200) == round(total_length))

    sf.write(os.path.join(args.output_dir, "wav_sanity_sample.wav"),
             np.squeeze(mulawdecoder(wav)), audio_prep_tool.final_sr)

    phone_ids = torch.load(phone_sample)
    phone_orig_length = phone_ids[0]
    phone_ids = phone_ids[1:]
    # print(phone_ids)
    phones = text_prep_tool.tensor_to_string(
        phone_ids[:int(phone_orig_length)])
    sanity_sample = open(os.path.join(
        args.output_dir, "txt_sanity_sample.txt"), "w", encoding="utf-8")
    sanity_sample.write(phones)
    sanity_sample.close()
    print("sanity check passed! you can listen to the audio and look at the transcription to check, too!")


def write_file_name_list(file_name_list, out_dir, sr):
    with open(os.path.join(out_dir, 'file_name_list.txt'), 'w', encoding='utf-8') as f:
        for file in file_name_list:
            f.write('|'.join([str(x).replace('.wav', '.pt').replace(
                ".txt", ".pt") for x in file]) + '\n')
        f.write(str(sr))
    # frames = sum([file[2] for file in file_name_list])
    # frame_shift_ms = hop_length * 1000 / sample_rate
    # hours = frames * frame_shift_ms / (3600 * 1000)
    # print('Write %d utterances, %d frames (%.2f hours)' %
    #      (len(file_name_list), frames, hours))


def main():
    parser = argparse.ArgumentParser()
    # parser.add_argument('--output', default='data')

    # LOCAL
    # parser.add_argument('--wav_dir', default='data/eng/16kHz/wav/')
    # parser.add_argument('--txt_dir', default='data/eng/txt/')

    # LOCAL TEST (SMALL)
    #parser.add_argument('--wav_dir', default='data/test/wav/')
    #parser.add_argument('--txt_dir', default='data/test/txt/')

    # SERVER
    parser.add_argument(
        '--wav_dir', default='/mount/resources/speech/corpora/LJSpeech/16kHz/wav/')
    parser.add_argument(
        '--txt_dir', default='/mount/resources/speech/corpora/LJSpeech/16kHz/txt/')

    parser.add_argument('--output_dir', default='src/data/')

    # can be 24k, 16k, 8k...
    parser.add_argument('--sr', type=int, default=24000)
    # can be en or de
    parser.add_argument('--language', default="en")

    args = parser.parse_args()

    preprocess(args)

    # wav_dir = "../../data/eng/16kHz/wav/"
    # txt_dir = "../../data/eng/txt/"
    # /data/test/wav
    # /data/test/txt


if __name__ == "__main__":
    main()

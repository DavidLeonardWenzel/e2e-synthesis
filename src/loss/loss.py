import torch
import torch.nn as nn
import torch.nn.functional as F
#from torchaudio.transforms import Spectrogram
import numpy as np
from loss.softdtwcuda import SoftDTW
from neural_nets.modules import ToMelSpectrogramLayer
from utils.misc import mu_law_encoding


def loss_hinge_dis(dis_fake, dis_real):
    # Hinge Loss
    loss_fake = torch.mean(F.relu(1. + dis_fake))
    loss_real = torch.mean(F.relu(1. - dis_real))
    return loss_fake, loss_real


def loss_hinge_gen(dis_fake):
    loss = -torch.mean(dis_fake)
    return loss


class LengthLoss(nn.Module):
    # this is the length loss as given in the EATS paper
    # L length =  1/2 (L − sum(predicted length)²
    # you can either train:
    # with no ground truth -> with DTW -> with this seq. length loss ( as done in EATS)
    # or with ground truth alignment -> optional DTW/STFT -> no length loss at all (as this should be zero, you are comparing the same values)
    # or with per-phone duration loss
    # note that neither lenght loss is predicted on the window, but on the whole utterance
    def __init__(self):
        super(LengthLoss, self).__init__()

    def forward(self, actual_lengths, predicted_lengths):
        """[length loss]
        for length loss per phone, see usage of MSE in train.py
        Args:
            predicted_lengths ([type]): the predicted token sequence length as calculated by the aligner
            actual_length ([type]): the number of time steps in the training utterance at 200 Hz

        Returns:
            [type]: [description]
        """
        return torch.mean(0.5 * (actual_lengths - predicted_lengths)**2)


def dtw_cost_function(x, y):
    diffs = abs(x[:, None, :, :] - y[:, :, None, :])
    # pairwise L1 cost, square the diffs for L2.
    # along time axis, all possible differencess
    cost = torch.mean(diffs, axis=-1)
    return cost


class DTWLoss(nn.Module):
    """Compute DTW error given a pair of spectrograms"""

    def __init__(self, use_cuda):
        super(DTWLoss, self).__init__()
        self.to_mel_spec_layer = ToMelSpectrogramLayer()
        self.softDTW = SoftDTW(
            gamma=0.01, dist_func=dtw_cost_function, use_cuda=use_cuda)

    def forward(self, real_wave, fake_wave):
        # todo check if i need to mu decode!
        real_wave_spec_batch = self.to_mel_spec_layer(
            real_wave, jitter=True, apply_mu_decode=True)
        fake_wave_spec_batch = self.to_mel_spec_layer(
            fake_wave, jitter=False, apply_mu_decode=True)

        return torch.mean(self.softDTW(real_wave_spec_batch, fake_wave_spec_batch))


def eats_spectrogram_dtw_error(real_spec, fake_spec, temperature=0.01, warp_penalty=1.0):
    """the pseudo-code given in the EATS-paper, translated to pytorch. note that i instead use the cuda-optimized stand-alone
        DTW in "softdtwcuda.py"

    Args:
        real_spec (torch tensor): the mel spectrogram of the target
        fake_spec (torch tensor): the mel spectrogram of the predicted wave
        temperature (float, optional): also called gamma, eases the DTW loss - the higher the easier. Defaults to 0.1.
        warp_penalty (float, optional): penalty for non-diagonal movement. Defaults to 1.0.

    Returns:
        [type]: [description]
    """
    # Compute cost matrix.
    # substract every value from every value
    diffs = abs(fake_spec[None, :, :] - real_spec[:, None, :])
    # pairwise L1 cost, square the diffs for L2.
    # along time axis, all differencess -> each possible alignment
    cost = torch.mean(diffs, axis=-1)
    size = cost.shape[-1]
    # Initialise path costs.
    path_cost = float('inf') * torch.ones(size + 1)
    path_cost_prev = float('inf') * torch.ones(size + 1)
    path_cost_prev[0] = 0.0
    # if both matrices are the same, the diagonal is 0, thus the best path is across the diagonal, where the total cost is 0
    # Aggregate path costs from cost[0, 0] to cost[-1, -1].
    cost = skew_matrix(cost)  # Shape is now (2 * size - 1, size).
    for i in range(2 * size - 1):
        directions = [path_cost_prev[:-1],  # goes one timestep forward, then one down, go to the next time step in both Sgen , Sgt
                      # another timestep to the right
                      path_cost[1:] + warp_penalty,
                      path_cost[:-1] + warp_penalty]  # go to the next time step in Sgen
        cost_of_best_paths = soft_minimum(directions, temperature)
        path_cost_next = cost[i] + cost_of_best_paths
        # path_cost_next = torch.tensor(1)
        path_cost_next = torch.cat(
            (torch.tensor(float('inf')).unsqueeze(0), path_cost_next))
        path_cost, path_cost_prev = path_cost_next, path_cost
    return path_cost[-1]


def soft_minimum(directions, temperature):
    # Compute the soft minimum with the given temperature
    # EATS pseudo-code:
    softmin = -temperature * \
        (torch.log(torch.sum(torch.exp(-directions / temperature))))
    # actual DTW as proposed by blondel et al.:
    # https://github.com/Sleepwalking/pytorch-softdtw/blob/master/soft_dtw.py
    # rmax = torch.max(torch.max(directions[0], directions[1]), directions[2])
    # rsum = torch.exp(directions[0] - rmax) + \
    #    torch.exp(directions[1] - rmax) + \
    #    torch.exp(directions[2] - rmax)
    # softmin = - temperature * (np.log(rsum) + rmax)
    return softmin


def skew_matrix(x):
    """ Skew a matrix so that the diagonals become the rows for easier DTW computation

    Args:
        x (torch tensor): the matrix

    Returns:
        torch tensor: matrix, skewed so that diagonals are rows. doubles the rows, keeps the column dimension
    """

    height, width = x.shape
    y = torch.zeros(height + width - 1, width)
    # number of diagonals in x, will be the rows after skewing
    for diagonal in range(height + width - 1):
        # Shift each column columns down by columns steps.
        for column in range(width):
            # torch clamp equivalent:
            # diag_column_diff = diagonal - column
            # new_row_index = 0
            # if (diag_column_diff >= 0) and (diag_column_diff <= height - 1):
            #    new_row_index = diag_column_diff
            # elif (diag_column_diff >= height-1):
            #    new_row_index = height-1
            # last case would be 0, but we init with 0s anyway
            # y[diagonal, column] = x[new_row_index, column]
            y[diagonal, column] = x[torch.clamp(torch.tensor(
                diagonal-column), torch.tensor(0), torch.tensor(height-1)), column]
    return y


def batch_skew_matrix(x):
    """as above, but for a batch of mel specs"""
    skewed_batch = []
    for batch in x:
        height, width = x.shape
        y = torch.zeros(height + width - 1, width)
        # number of diagonals in x, will be the rows after skewing
        for diagonal in range(height + width - 1):
            # Shift each column columns down by columns steps.
            for column in range(width):
                # torch clamp is equivalent:
                # diag_column_diff = diagonal - column
                # new_row_index = 0
                # if (diag_column_diff >= 0) and (diag_column_diff <= height - 1):
                #    new_row_index = diag_column_diff
                # elif (diag_column_diff >= height-1):
                #    new_row_index = height-1
                # last case would be 0, but we init with 0s anyway
                # y[diagonal, column] = x[new_row_index, column]
                y[diagonal, column] = x[torch.clamp(torch.tensor(
                    diagonal-column), torch.tensor(0), torch.tensor(height-1)), column]
        skewed_batch.append(y)
    return torch.stack(skewed_batch)


class STFTLoss(nn.Module):
    def __init__(self,
                 device,
                 fft_size=1024,
                 hop_size=120,
                 win_size=600):
        super(STFTLoss, self).__init__()

        self.device = device
        self.fft_size = fft_size
        self.hop_size = hop_size
        self.win_size = win_size
        self.window = torch.hann_window(win_size).to(self.device)
        self.sc_loss = SpectralConvergence()
        self.mag = LogSTFTMagnitude()

    def forward(self, predicts, targets):
        """
        Args:
            x: predicted signal(B, T).
            y: truth signal(B, T).

        Returns:
            Tensor: STFT loss values.
        """
        predicts_mag = stft(predicts, self.fft_size,
                            self.hop_size, self.win_size, self.window)
        targets_mag = stft(targets, self.fft_size,
                           self.hop_size, self.win_size, self.window)

        sc_loss = self.sc_loss(predicts_mag, targets_mag)
        mag_loss = self.mag(predicts_mag, targets_mag)

        return sc_loss, mag_loss


class MultiResolutionSTFTLoss(nn.Module):
    def __init__(self,
                 device,
                 fft_sizes=[1024, 2048, 512],
                 win_sizes=[600, 1200, 240],
                 hop_sizes=[120, 240, 50]):
        super(MultiResolutionSTFTLoss, self).__init__()
        self.loss_layers = torch.nn.ModuleList()
        self.device = device
        for (fft_size, win_size, hop_size) in zip(fft_sizes, win_sizes, hop_sizes):
            self.loss_layers.append(
                STFTLoss(device, fft_size, hop_size, win_size))

    def forward(self, fake_signals, true_signals):
        sc_losses, mag_losses = [], []
        for layer in self.loss_layers:
            sc_loss, mag_loss = layer(fake_signals, true_signals)
            sc_losses.append(sc_loss)
            mag_losses.append(mag_loss)

        sc_loss = sum(sc_losses) / len(sc_losses)
        mag_loss = sum(mag_losses) / len(mag_losses)

        return sc_loss, mag_loss


def stft(x, fft_size, hop_size, win_size, window):
    """Perform STFT and convert to magnitude spectrogram.

    Args:
        x: Input signal tensor(B, T).

    Returns:
        Tensor: Magnitude spectrogram(B, T, fft_size // 2 + 1).

    """
    x_stft = torch.stft(x, fft_size, hop_size, win_size, window)
    real = x_stft[..., 0]
    imag = x_stft[..., 1]
    outputs = torch.clamp(real ** 2 + imag ** 2, min=1e-7).transpose(2, 1)
    outputs = torch.sqrt(outputs)

    return outputs


class SpectralConvergence(nn.Module):
    def __init__(self):
        """Initilize spectral convergence loss module."""
        super(SpectralConvergence, self).__init__()

    def forward(self, predicts_mag, targets_mag):
        x = torch.norm(targets_mag - predicts_mag, p='fro')
        y = torch.norm(targets_mag, p='fro')

        return x / y


class LogSTFTMagnitude(nn.Module):
    def __init__(self):
        super(LogSTFTMagnitude, self).__init__()

    def forward(self, predicts_mag, targets_mag):
        log_predicts_mag = torch.log(predicts_mag)
        log_targets_mag = torch.log(targets_mag)
        outputs = F.l1_loss(log_predicts_mag, log_targets_mag)

        return outputs

import argparse
# from StandaloneDurationPredictor.ProcessText import TextFrontend
# from StandaloneDurationPredictor.StandaloneDurationPredictor import StandaloneDurationPredictor
from utils.PreprocessingForTTS import ProcessText
import soundfile
import json
import torch
import os
from utils.misc import pad_phones_and_durs, write, to_timestep, read_to_file
from neural_nets.EATS_Generator import Generator
import matplotlib.pyplot as plt
import librosa.display as lbd
from loss.loss import ToMelSpectrogramLayer


def attempt_to_restore(generate, checkpoint_dir):
    checkpoint_list = os.path.join(checkpoint_dir, 'checkpoint')

    if os.path.exists(checkpoint_list):
        checkpoint_filename = open(checkpoint_list).readline().strip()
        checkpoint_path = os.path.join(
            checkpoint_dir, "{}".format(checkpoint_filename))
        print("Restore from {}".format(checkpoint_path))
        checkpoint = load_checkpoint(checkpoint_path)
        generate.load_state_dict(checkpoint["generator"])
    else:
        print("no checkpoints found")


def load_checkpoint(checkpoint_path):
    # if cuda:
        # checkpoint = torch.load(checkpoint_path)
    checkpoint = torch.load(
        checkpoint_path, map_location=lambda storage, loc: storage)
    return checkpoint


def sample(experiment_number, dur_overwrite=False):
    if dur_overwrite:
        # causes aligner to overwrite predicted lenght values with ground truth
        ground_truth_alignment = True
    else:
        ground_truth_alignment = False
    #########################################################
    # CUDA configuration
    #########################################################
    if args.gpu_id == "cpu":
        os.environ["CUDA_VISIBLE_DEVICES"] = ""
        device = torch.device("cpu")
    else:
        os.environ["CUDA_DEVICE_ORDER"] = "PCI_BUS_ID"
        os.environ["CUDA_VISIBLE_DEVICES"] = "{}".format(args.gpu_id)
        device = torch.device("cuda")
    text_prep_tool = ProcessText.TextFrontend(language="en",
                                              use_panphon_vectors=False,
                                              use_explicit_eos=True,
                                              path_to_panphon_table="src/utils/PreprocessingForTTS/ipa_vector_lookup.csv")

    generator = Generator(
        device=device, sample=True, ground_truth_alignment=ground_truth_alignment).to(device)

    attempt_to_restore(
        generator, "logdir/run_{}/checkpoints".format(experiment_number))

    batch_size = 1

    continuing = True
    while continuing:
        print("What do you want me to say?")
        user_input = input()
        if user_input == "stop" or user_input == "exit":
            continuing = False
        else:
            print("synthesizing...")
            with torch.no_grad():
                raw_user_input = user_input
                user_input = text_prep_tool.string_to_tensor(
                    user_input).squeeze(0).long()[:600]
                phone_tensors, durs, start_pad_value = pad_phones_and_durs(
                    user_input, torch.zeros_like(user_input), 600)

                pad_start_values = torch.Tensor(
                    [start_pad_value]).long().to(device).detach()
                token_sequences = phone_tensors.unsqueeze(dim=0).to(device)
                phone_durations = durs.unsqueeze(dim=0).to(device).detach()
                noise = torch.randn(batch_size, 128).to(device)
                out_offset = torch.Tensor([0]).to(device).detach()

                # OVERWRITE INPUT TO TEST
                if dur_overwrite:
                    phone_alignments = read_alignment_file("alignments.json")
                    phones, phone_durations = get_phones_and_durations(
                        phone_alignments, "data/test/wav/LJ001-0003.wav")
                    phones.append(1)  # EOS token!
                    raw_user_input = phones
                    # get the durations in timesteps in 200Hz:
                    phone_durations = to_timestep(phone_durations, 200)
                    phones = torch.tensor(phones)
                    phone_durations = torch.tensor(phone_durations)
                    padded_phones, padded_durs, start_pad_value = pad_phones_and_durs(
                        phones, phone_durations, 600)
                    token_sequences = padded_phones.unsqueeze(dim=0)
                    phone_durations = padded_durs.unsqueeze(dim=0)
                    pad_start_values = torch.tensor([start_pad_value])

                upsampled_aligned_phones, predicted_token_lengths = generator(
                    tokens=token_sequences,
                    lengths=pad_start_values,
                    noise=noise,
                    out_offset=out_offset,
                    ground_truth_token_lengths=phone_durations)

                try:
                    upsampled_aligned_phones = cut_padding(
                        upsampled_aligned_phones, predicted_token_lengths)
                except:
                    print("unable to crop output due to low length predictions.")

                file_loc = "logdir/run_{}/samples/".format(
                    experiment_number)
                upsampled_aligned_phones = upsampled_aligned_phones.squeeze()
                #upsampled_aligned_phones_cut = upsampled_aligned_phones_cut.squeeze()

                read_to_file(upsampled_aligned_phones,
                             os.path.join(file_loc, "sentence.wav"))
                sample_visualize_output(text_prep_tool, upsampled_aligned_phones,
                                        os.path.join(file_loc, "vis.png"), raw_user_input)
                write(os.path.join(file_loc, "durations.txt"),
                      str(raw_user_input)+"\n"+str(predicted_token_lengths))
                # read_aloud("test.wav")
            print("done!")


def cut_padding(upsampled_aligned_phones, predicted_token_length):
    cut_point_upsampled = int(torch.round(
        ((torch.sum(predicted_token_length, dim=1)) / 200)*24000))
    upsampled_aligned_phones = upsampled_aligned_phones[:,
                                                        :, :cut_point_upsampled]
    print("cutting to {} seconds!".format(cut_point_upsampled/24000))
    return upsampled_aligned_phones


def get_phones_and_durations(phone_alignments, file_name):
    base_path = "/mount/resources/speech/corpora/LJSpeech/16kHz/wav/"
    return phone_alignments[base_path + file_name.split("/")[-1]]


def read_alignment_file(path):
    with open(path) as json_file:
        data = json.load(json_file)
    return data


def sample_visualize_output(text_prep_tool, wav, file_loc, text):
    spec = ToMelSpectrogramLayer()(wav, jitter=False, apply_mu_decode=False)

    fig, ax = plt.subplots(nrows=2, ncols=1, figsize=(6, 3))
    ax[0].plot(wav.cpu().numpy())
    lbd.specshow(spec.numpy(), ax=ax[1], sr=24000, cmap='GnBu', y_axis='mel', x_axis='time',
                 hop_length=1024)
    ax[0].set_title(text_prep_tool.get_phone_string(text))
    # ax[0].yaxis.set_visible(False)
    # ax[1].yaxis.set_visible(False)

    plt.subplots_adjust(left=0.07, bottom=0.1,
                        right=0.95, top=.9, wspace=0.0, hspace=0.0)
    plt.show()
    plt.savefig(file_loc)
    # print("plotted!")


if __name__ == '__main__':

    parser = argparse.ArgumentParser()

    # if not cpu, uses cuda instead!
    parser.add_argument('--gpu_id', type=str, default="cpu")

    args = parser.parse_args()

    experiment_number = 6
    sample(experiment_number, dur_overwrite=False)
    print("Thank you for EATSing with us. \nGoodbye~")
    #dur_overwrite = False

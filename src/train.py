import time
import argparse

import torch.nn as nn
from torch.nn import MSELoss
from loss.loss import loss_hinge_gen, loss_hinge_dis, DTWLoss, MultiResolutionSTFTLoss, LengthLoss
from torch.nn import HingeEmbeddingLoss
from utils.misc import ortho, write, log_step, save_checkpoint, count_parameters, attempt_to_restore
from tensorboardX import SummaryWriter
from torch.optim.lr_scheduler import CosineAnnealingLR as Scheduler
import torch.optim as optim
import torch
from utils.dataload import CustomDataset
from torch.utils.data import DataLoader
import os
from neural_nets.GAN_TTS_Discriminator import Multiple_Random_Window_Discriminators
from neural_nets.WAVEGAN_Discriminator import WAVEGAN_Discriminator
from neural_nets.EATS_Generator import Generator
# from neural_nets.BIGGANDeep_Discriminator import Discriminator as BIGGANDeep_Discriminator

from neural_nets.modules import ToMelSpectrogramLayer


import random


def train():
    #########################################################
    # CUDA configuration
    #########################################################
    if args.gpu_id == "cpu":
        os.environ["CUDA_VISIBLE_DEVICES"] = ""
        device = torch.device("cpu")
    else:
        os.environ["CUDA_DEVICE_ORDER"] = "PCI_BUS_ID"
        os.environ["CUDA_VISIBLE_DEVICES"] = "{}".format(args.gpu_id)
        device = torch.device("cuda")
        # torch.cuda.empty_cache()
        # torch.cuda.set_enabled_lms(True)
        # torch.cuda.set_size_lms(size)
    print("loading models, starting on device", device)
    torch.manual_seed(11)
    # random.seed(1)
    # faster if input size is always the same, and we pad input size to be the same / cut audio to be the same
    torch.backends.cudnn.benchmark = True

    #######################################################
    # load dataset:
    #######################################################
    train_dataset = CustomDataset()

    #######################################################
    # initialize neural nets:
    #######################################################
    generator = Generator(
        device=device, sample=False, ground_truth_alignment=args.ground_truth_alignment).to(device)
    # count_parameters(generator)
    # spec_discriminator = BIGGANDeep_Discriminator()  # unused for now, too big
    # count_parameters(spec_discriminator)
    if args.discriminator_version == "GAN_TTS":  # default option
        discriminator = Multiple_Random_Window_Discriminators().to(device)
    else:
        print("using discriminator from parallel wavegan")
        discriminator = WAVEGAN_Discriminator().to(device)

    # count_parameters(generator)
    #################################
    # hyperparameter
    ################################
    """both updates are computed using the Adam optimizer with
    - β 1 = 0 and β 2 = 0.999,
    - learning rate of 10 −3
    - cosine decay schedule used such that the learning rate is 0 at step 500K.
    """
    g_learning_rate = 0.001
    d_learning_rate = 0.001
    batch_size = args.batch_size
    # batch_size = 1024  #value in paper, only feasable with TPU+bigger dataset
    # optimizers:
    g_parameters = list(generator.parameters())
    g_optimizer = optim.Adam(
        g_parameters,
        lr=g_learning_rate,
        # lr_decay=0 and l2 weight_decay is not given -> instead, scheduler and ortho regu
        betas=(0, 0.999))

    d_parameters = list(discriminator.parameters())
    d_optimizer = optim.Adam(
        d_parameters,
        lr=d_learning_rate,
        # lr_decay=0,  # cosine decay schedule used such that the learning rate is 0 at step 500K
        # l2 weight_decay ->
        betas=(0, 0.999))

    # https://discuss.pytorch.org/t/how-to-implement-torch-optim-lr-scheduler-cosineannealinglr/28797/28
    # cosine decay schedule used such that the learning rate is 0 at step 500K
    g_scheduler = Scheduler(
        optimizer=g_optimizer, T_max=args.train_steps)  # default 500.000
    d_scheduler = Scheduler(
        optimizer=d_optimizer, T_max=args.train_steps)  # default 500.000
    # carefull: when doing g warmup, the d_scheduler will decay too, meaning the discriminator starts with a lower LR

    global_step = 0
    use_cuda = True if args.gpu_id != "cpu" else False
    if args.restore is not None:
        restore_step = attempt_to_restore(generator, discriminator, g_optimizer,
                                          d_optimizer, g_scheduler, d_scheduler, args.restore, use_cuda)
        global_step = restore_step

    ###########################
    # Losses
    ###########################
    # when training normal EATS, i.e, without ground truth duration, use:
    # dtw_loss_func = DTWLoss(use_cuda).to(device)
    soft_dtw_func = DTWLoss(use_cuda=use_cuda).to(device)
    eats_length_loss_func = LengthLoss().to(device)
    length_loss_per_phone_func = MSELoss().to(device)
    adv_hinge_loss_func = nn.HingeEmbeddingLoss().to(device)
    # optionally, use:
    stft_loss_func = MultiResolutionSTFTLoss(device).to(device)

    lambda_pred = 1.0    # DTW
    lambda_length = 0.001  # LENGTH LOSS, paper gives 0.01
    lambda_stft = 2.0    # SC + MAG LOSS
    lambda_adv = 4.0     # DISC LOSS

    ###########################
    # misc.
    ###########################
    writer = SummaryWriter(args.logdir)

    # accumulation_steps = 3  # for gradient accumulation to get to higher batch size

    # collate = CustomCollate()

    train_data_loader = DataLoader(
        train_dataset, batch_size=batch_size, shuffle=True, pin_memory=True, drop_last=True)
    # ljspeech training data set is about ~13000
    # batch size in paper: 1024, per epoch 12,7 steps
    # batch size default is 64: per epoch 203 steps
    # batch size NVIDIA GeForce RTX 3090 is 24
    # batch size NVIDIA GeForce RTX 2080 is 10

    # begin train loop:
    print("loaded data, starting training")
    while True:
        for _step, (wav_tensors, wav_orig_length, wav_cut_value, phone_tensors, pad_start_values, phone_duration_tensors) in enumerate(train_data_loader):
            start = time.time()
            # loss_sum = 0
            # print("setting sum to 0!")
            g_optimizer.zero_grad()
            d_optimizer.zero_grad()
            # for i in range(accumulation_steps):
            losses = {}
            #############################################
            # put all tensors that will be used to device!
            #############################################
            # main input:
            noise = torch.randn(batch_size, 128).to(device)
            token_sequences = phone_tensors.to(device)
            waves = wav_tensors.to(device)

            # auxialliary inputs for the aligner:
            phones_start_pad_value = pad_start_values.to(device).detach()
            out_offset = wav_cut_value.to(device).detach()

            # for training with ground truth:
            phone_durations = phone_duration_tensors.to(device).detach()

            # for the EATS length loss, should match sum(predicted_token_length) and does match sum(phone_durations)
            actual_sequence_length = wav_orig_length.to(device).detach()

            upsampled_aligned_phones, predicted_token_lengths = generator(
                tokens=token_sequences,
                lengths=phones_start_pad_value,
                noise=noise,
                out_offset=out_offset,
                ground_truth_token_lengths=phone_durations)

            if global_step >= args.discriminator_train_start_steps:
                ###########################
                # Train Discriminator:
                ###########################
                # wave form based discriminators:
                d_out_real_data = discriminator(waves.unsqueeze(dim=1))
                d_out_generated_data = discriminator(
                    upsampled_aligned_phones.detach())  # doesn't influence generator when training discriminator!

                # spectrogram based discriminators:
                # pred_out_real_data = spec_discriminator(
                #    x=waves, jitter=True)
                # pred_out_fake_data = spec_discriminator(
                #    x=upsampled_aligned_phones.squeeze().detach(), jitter=False)

                ###########################
                # Adversarial Loss for Disc.:
                ###########################
                # returns the list of 5 discriminators, thus we collect and sum the loss!
                if args.discriminator_version == "GAN_TTS":
                    fake_adv_loss = []
                    real_adv_loss = []

                    # hinge embedding loss:
                    # FAKE discs:
                    # for each_discriminator in d_out_generated_data:
                    #    fake_adv_loss.append(adv_hinge_loss_func(
                    #        each_discriminator, torch.zeros_like(each_discriminator, device=device).fill_(-1)))
                    #fake_adv_loss = sum(fake_adv_loss)

                    # REAL discs:
                    # for each_discriminator in d_out_real_data:
                    #    real_adv_loss.append(adv_hinge_loss_func(
                    #        each_discriminator, torch.ones_like(each_discriminator, device=device)))
                    #real_adv_loss = sum(real_adv_loss)
                    # big gan negative mean hinge implementation:
                    for fake, real in zip(d_out_generated_data, d_out_real_data):
                        fake_loss, real_loss = loss_hinge_dis(fake, real)
                        fake_adv_loss.append(fake_loss)
                        real_adv_loss.append(real_loss)

                    fake_adv_loss = sum(fake_adv_loss)
                    real_adv_loss = sum(real_adv_loss)

                else:  # uses parallel wavegan discriminator:
                    fake_adv_loss = adv_hinge_loss_func(
                        d_out_generated_data, torch.zeros_like(d_out_generated_data, device=device).fill_(-1))
                    real_adv_loss = adv_hinge_loss_func(
                        d_out_generated_data, torch.ones_like(d_out_real_data, device=device))

                discriminator_loss = fake_adv_loss + real_adv_loss

                # d_optimizer.zero_grad()  # stops floating background gradients to zero, called at start of step!
                discriminator_loss.backward()  # then do backward pass!

                # ortho not used for discriminators::
                # updates conv parameters inplace to keep them off-diagonal
                # ortho(discriminator) #not used in biggan and eats (or with weight of 0...)

                # if gradients are too big, clips it, stops exploding gradient
                # nn.utils.clip_grad_norm_(d_parameters, max_norm=0.5)

                d_optimizer.step()

            else:  # before global_step > args.discriminator_train_start_steps, the loss is 0:
                discriminator_loss = torch.Tensor([0])
                fake_adv_loss = torch.Tensor([0])
                real_adv_loss = torch.Tensor([0])

            # decrease the LR even when not training so it is in sync with g lr
            d_scheduler.step()
            # save losses for logging / restoring
            losses['fake_loss'] = fake_adv_loss.item()
            losses['real_loss'] = real_adv_loss.item()
            losses['discriminator_loss'] = discriminator_loss.item()
            #########################
            # train the GENERATOR
            #########################
            d_out_generated_data = discriminator(
                upsampled_aligned_phones.float())

            # spec_d_out_generated_data = spec_discriminator(
            #    upsampled_aligned_phones.squeeze(), jitter=True)  # set to false after debugging
            # TODO if including biggan disc, how does gradient flow  work with multiple discs?
            # input a clone and keep history of both?

            ######################
            # Adversarial Loss:
            ######################
            if args.discriminator_version == "GAN_TTS":
                # sum the loss of each discriminator!
                adv_loss = []
                # sum the loss of each discriminator!
                adv_loss = []
                for each_discriminator in d_out_generated_data:
                    #adv_loss.append(adv_hinge_loss_func(each_discriminator, torch.ones_like(each_discriminator, device=device)))
                    # big gan negative mean hinge loss implementation:
                    adv_loss.append(loss_hinge_gen(each_discriminator))
                adv_loss = sum(adv_loss)
            # summing losses like this is equivalent to:
            # adv_loss = loss_hinge_gen(sum(d_outs_generated_data))
            # GAN-TTS shows the summation of discriminator outputs (p.6), but summing is only possible if the outputs
            # have the same shape (and the 5th discriminator doesn't)
            # padding causes the mean to go down, so i calculate the adv. loss as above, i.e, first calculating the loss per D, then summing
            # as all operations are summing, the order does change the result
            # if the discriminator is from parallel wavegan, the outputs are different (single, not one that is summed), thus:
            else:  # for parallel wavegan discriminator, just take the loss:
                adv_loss = adv_hinge_loss_func(
                    d_out_generated_data, torch.ones_like(d_out_generated_data, device=device))

            ########################
            # Auxiliary Losses:
            ########################
            # we need a DTW and Length loss to kickstart training
            # optionally add STFT loss to help

            #####################
            # Length Loss:
            #####################
            if args.eats_length_loss:
                # original eats length loss, does not need ground truth, takes sum of utterance instead
                length_loss = eats_length_loss_func(
                    actual_sequence_length, torch.sum(predicted_token_lengths, dim=1), )

            else:  # with ground truth + masked predicted length per phone, we can instead take the MSE:
                # if global_step >= args.discriminator_train_start_steps:
                #    length_loss = eats_length_loss_func(
                #        actual_sequence_length, torch.sum(predicted_token_lengths, dim=1))
                # else:
                length_loss = length_loss_per_phone_func(
                    phone_durations, predicted_token_lengths, )
            #####################
            # DTW Loss
            #####################
            # spec_a = ToMelSpectrogramLayer(
            #    apply_mu_decode=False)(waves, jitter=True)
            # spec_b = ToMelSpectrogramLayer(apply_mu_decode=False)(
            #   , jitter=False)

            dtw_loss = soft_dtw_func(waves, upsampled_aligned_phones.squeeze())
            # dtw_loss = torch.Tensor([0]).to(device)

            if args.stft_loss:
                sc_loss, mag_loss = stft_loss_func(
                    upsampled_aligned_phones.squeeze(1), waves.squeeze(1))
            else:
                sc_loss = torch.Tensor([0.000]).to(device)
                mag_loss = torch.Tensor([0.000]).to(device)

            ####################
            # LOSSES, ASSEMBLE!
            ####################

            if global_step >= args.discriminator_train_start_steps:
                # Combination of losses with adv loss and lambda weights, after kickstarting phase:
                # as soon as we start training and updating the discriminator, we add the adversarial loss:
                # EATS is:
                # LG = LG,adv + λpred (a factor of 1.0) · L''pred + λlength (a factor of 0.1) · L length

                g_loss = (lambda_adv * adv_loss) + \
                    (lambda_pred * dtw_loss) + \
                    (lambda_length * length_loss) + \
                    (lambda_stft * (sc_loss + mag_loss))
                # notice that sc and mag is both 0 when training EATS-like

            # combination before kickstarting, without adversarial loss (when discriminator is not being updated)
            else:
                g_loss = (lambda_pred * dtw_loss) + \
                    (lambda_length * length_loss) + \
                    (lambda_stft * (sc_loss + mag_loss))

            losses['adv_loss'] = adv_loss.item()
            losses['sc_loss'] = sc_loss.item()
            losses['mag_loss'] = mag_loss.item()
            losses['dtw_loss'] = dtw_loss.item()
            losses['g_loss'] = g_loss.item()
            losses['length_loss'] = length_loss.item()

            # g_loss = g_loss / accumulation_steps
            # print("gloss of current forward,divided, is", g_loss)
            # print("summing loss, backward number", i)
            g_loss.backward()
            # loss_sum += g_loss
            # print("sum is:")
            # print(loss_sum)
            # Reset gradients, for the next accumulated batches

            # nn.utils.clip_grad_norm_(g_parameters, max_norm=0.5)
            # done with accumulation
            # print("step!")
            ortho(generator)
            g_optimizer.step()
            g_scheduler.step()
            time_used = time.time() - start

            if global_step >= args.discriminator_train_start_steps:
                print("Step: {} --adv_loss: {:.3f} --real_loss: {:.3f} --fake_loss: {:.3f} --sc_loss: {:.3f} --mag_loss: {:.3f} --dtw_loss: {:.3f} --length_loss: {:.3f} --Time: {:.2f} seconds".format(
                    global_step, adv_loss, real_adv_loss, fake_adv_loss, sc_loss.item(), mag_loss.item(), dtw_loss, length_loss, time_used))
            else:
                print("Step: {} --sc_loss: {:.3f} --mag_loss: {:.3f} --dtw_loss: {:.3f} --length_loss: {:.3f} --Time: {:.2f} seconds".format(
                    global_step, sc_loss.item(), mag_loss.item(), dtw_loss, length_loss, time_used))

            global_step += 1

            if global_step % args.checkpoint_step == 0:
                save_checkpoint(args.checkpoint_dir, generator, discriminator,
                                g_optimizer, d_optimizer, g_scheduler, d_scheduler, global_step)
                if global_step > args.train_steps:
                    print("Done training!")
                    return
            if global_step % args.summary_step == 0:
                for key in losses:
                    writer.add_scalar('{}'.format(
                        key), losses[key], global_step)
            if global_step % args.log_step == 0:
                with torch.no_grad():
                    log_step(global_step=global_step, predicted_token_lengths=predicted_token_lengths,
                             actual_sequence_length=actual_sequence_length, waves=waves, upsampled_aligned_phones=upsampled_aligned_phones, ToMelSpectrogramLayer=ToMelSpectrogramLayer)


if __name__ == "__main__":
    torch.set_printoptions(precision=3, sci_mode=False)

    def _str_to_bool(s):
        """Convert string to bool(in argparse context)."""
        if s.lower() not in ['true', 'false']:
            raise ValueError('Argument needs to be a '
                             'boolean, got {}'.format(s))
        return {'true': True, 'false': False}[s.lower()]

    parser = argparse.ArgumentParser()
    parser.add_argument('--input', type=str, default='data/train',
                        help='Directory of training data')
    parser.add_argument('--discriminator_train_start_steps',
                        type=int, default=0)  # ~100.000
    # note that EATS gives no warmup details, and by the time disc gets updated, the disc LR is low due to scheduling!
    parser.add_argument('--discriminator_version',
                        type=str, default="GAN_TTS"),  # if not GAN_TTS, system uses discriminator of parallelwavegan
    parser.add_argument('--train_steps', type=int, default=500000)
    parser.add_argument('--ground_truth_alignment',
                        type=_str_to_bool, default=True)  # when true, multiply in the aligner with the ground truth timesteps
    parser.add_argument('--eats_length_loss',                # if true, needs groundtruth phone duration
                        type=_str_to_bool, default=True)  # when true, uses eats length loss instead of per phone MSE
    # if false allows training without ground truth!
    # when false, use ground truth only to warm-up generator by improving the length loss
    parser.add_argument('--stft_loss',                # if true, needs groundtruth phone duration
                        type=_str_to_bool, default=False)
    # when true, adds an additional loss from parallel wave gan, not found in original EATS

    # for batch size, try 1024 for original paper, needs immense GPU memory. mine fits 24 max!
    parser.add_argument('--batch_size', type=int, default=24)
    parser.add_argument('--log_step', type=int, default=10000,
                        help="At which step to print visual representation and write wav training samples down")

    parser.add_argument('--checkpoint_dir', type=str,
                        default="checkpoints", help="Directory to save model")
    parser.add_argument('--logdir', type=str,
                        default="logdir", help="Directory to save logs")
    parser.add_argument('--restore', type=str, default=None,
                        help="The model name to restore")
    parser.add_argument('--checkpoint_step', type=int, default=50000)
    parser.add_argument('--summary_step', type=int, default=50000)

    # if not cpu, uses cuda instead!
    parser.add_argument('--gpu_id', type=str, default="cpu")

    args = parser.parse_args()
    # args.restore = "model.ckpt-200000.pt"
    train()

from neural_nets.EATS_Aligner import Aligner
from neural_nets.GAN_TTS_Generator import GAN_TTS_Generator
import torch.nn as nn
from utils.misc import count_parameters

# This Generator uses the Aligner and then the GAN-TTS Generator to decode and upsample the Aligner output


class Generator(nn.Module):
    def __init__(self, device, sample, ground_truth_alignment):
        super(Generator, self).__init__()
        self.device = device
        self.sample = sample
        self.ground_truth_alignment = ground_truth_alignment
        self.aligner = Aligner(
            device=self.device,
            sample=self.sample,
            ground_truth_alignment=self.ground_truth_alignment)
        self.decoder = GAN_TTS_Generator(
            256, 128, sample=sample)

        #print("aligner modules")
        # print(len(list(self.aligner.parameters())))
        #print("aligner params")
        # count_parameters(self.aligner)
        #print("decoder modules")
        # print(len(list(self.decoder.parameters())))
        #print("decoder params:")
        # count_parameters(self.decoder)

    def forward(self, tokens, lengths, noise, out_offset, ground_truth_token_lengths):
        aligned_features, predicted_token_lengths = self.aligner(
            tokens=tokens,
            lengths=lengths,
            noise=noise,
            out_offset=out_offset,
            ground_truth_token_lengths=ground_truth_token_lengths
        )

        upsampled_output = self.decoder(
            aligned_features, noise)

        return upsampled_output, predicted_token_lengths

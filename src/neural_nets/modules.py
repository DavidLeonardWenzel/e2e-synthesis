import torch.nn as nn
from torch.nn.utils import spectral_norm
import torch
import torch.nn.functional as F
from torch.nn import Parameter as P
from torchaudio.transforms import MelSpectrogram
from torchaudio.transforms import MuLawDecoding
from utils.misc import RandomCrop


class ConditionalBatchNorm1d(nn.Module):
    """ Conditional Batch Normalization with Linear activation on the condition as the embedding,
        does not apply spectral normalization, with relu activation"""

    def __init__(self, num_features, z_channels=128):
        super().__init__()

        self.num_features = num_features
        self.z_channels = z_channels
        self.batch_norm = nn.GroupNorm(
            num_channels=num_features, affine=False, num_groups=32)
        # self.batch_norm = nn.BatchNorm1d(
        #    num_features=num_features, affine=False)

        self.layer = nn.Linear(z_channels, num_features * 2)
        # Initialise scale at N(1, 0.02)
        # https://discuss.pytorch.org/t/affine-parameter-in-batchnorm/6005/3
        self.layer.weight.data.normal_(1, 0.02)
        self.layer.bias.data.zero_()             # Initialise bias at 0

    def forward(self, inputs, noise, relu=True):
        # N,C,L:
        outputs = self.batch_norm(inputs)
        gamma, beta = self.layer(noise).chunk(2, 1)
        gamma = gamma.view(-1, self.num_features, 1)
        beta = beta.view(-1, self.num_features, 1)
        outputs = gamma * outputs + beta
        return F.leaky_relu(outputs)
        # return F.relu(outputs)


class SpectralConditionalBatchNorm1d(nn.Module):
    """ Conditional Batch Normalization with Linear activation on the condition,
        does apply spectral normalization"""

    def __init__(self, num_features, z_channels=128):
        super().__init__()

        self.num_features = num_features
        self.z_channels = z_channels
        self.batch_norm = nn.GroupNorm(
            num_channels=num_features, affine=False, num_groups=32)
        # self.batch_norm = nn.BatchNorm1d(
        #   num_features=num_features, affine=False)

        self.layer = spectral_norm(nn.Linear(z_channels, num_features * 2))
        # Initialise scale at N(1, 0.02)
        self.layer.weight.data.normal_(1, 0.02)
        self.layer.bias.data.zero_()             # Initialise bias at 0

    def forward(self, inputs, noise):
        outputs = self.batch_norm(inputs)
        gamma, beta = self.layer(noise).chunk(2, 1)
        gamma = gamma.view(-1, self.num_features, 1)
        beta = beta.view(-1, self.num_features, 1)

        outputs = gamma * outputs + beta

        return outputs


class OrthogonalMaskedConv1d(nn.Module):
    """non-causal, dilated Conv1d with orthogonal initialisation but without spectral normalisation, for the aligner
    after convolution, masks / mutes "swept over" values that were padded with a mask again"""

    def __init__(self,
                 in_channels,
                 out_channels,
                 dilation,
                 kernel_size,
                 stride=1,
                 groups=1):
        super(OrthogonalMaskedConv1d, self).__init__()

        self.in_channels = in_channels
        self.out_channels = out_channels
        self.dilation = dilation
        self.kernel_size = kernel_size
        self.stride = stride
        self.groups = groups
        padding = (self.kernel_size-1) * dilation // 2
        # for causal padding, do:
        # causal_padding = (kernel_size - 1) * dilation
        # you use causal when you do regressive nets, but generally dont want do
        # thus we do non causal padding
        self.layer = nn.Conv1d(in_channels, out_channels, kernel_size=self.kernel_size,
                               stride=stride, padding=padding, dilation=dilation, groups=groups)
        nn.init.orthogonal_(self.layer.weight)

    def forward(self, inputs, conv_mask):
        """Since our generator is a fully-convolutional network, in theory it is capable of generating samples
        of arbitrary length. However, different lengths are zero-padded for batch optimization, to
        fit in a fixed-size tensor. Convolutional layers pad their inputs to create outputs of the desired dimensionality,
        hence we need to ensure that the padded part of the input tensors to all layers is always zero.
        Convolutions(with kernel sizes greater than one) would propagate non-zero values outside the border
        between meaningful input and padding. A simple way to address this issue is masking,
        i.e. multiplying the input by a zero-one mask tensor, directly before each convolutional layer.
        This enables batched sampling of utterances of different length, which is efficient on many hardware platforms,
        optimised for batching."""

        inputs = self.layer(inputs)
        # then applies mask, 0 the originally padded values again
        return torch.mul(inputs, conv_mask)
        # return torch.mul(inputs.transpose(1,2), conv_mask).transpose(1,2)


class OrthogonalConv1d(nn.Module):
    """non-causal, non-dilated Conv1d with orthogonal initialisation but without spectral normalisation, for the aligner
    same as maskedConv1d, but without the masking, since there is no dilation!"""

    def __init__(self,
                 in_channels,
                 out_channels,
                 kernel_size,
                 dilation=1,
                 stride=1,
                 groups=1):
        super(OrthogonalConv1d, self).__init__()

        self.in_channels = in_channels
        self.out_channels = out_channels
        self.dilation = dilation
        self.kernel_size = kernel_size
        self.stride = stride
        self.groups = groups
        pad = dilation * (kernel_size - 1) // 2
        layer = nn.Conv1d(in_channels, out_channels, kernel_size=kernel_size,
                          stride=stride, padding=pad, dilation=dilation, groups=groups)
        nn.init.orthogonal_(layer.weight)
        self.layer = layer

    def forward(self, inputs):
        return self.layer(inputs)


class SpectralConv1d(nn.Module):
    """Conv1d for spectral normalisation and orthogonal initialisation
    No masking, but with spectral norm.:
    We apply spectral normalisation (Miyato et al., 2018) to the weights of the generator’s decoder module and to the
    discriminators (but not to the generator’s aligner module)
    thus, this is used in the DECODER and DISCRIMINATOR
    """

    def __init__(self,
                 in_channels,
                 out_channels,
                 kernel_size=1,
                 stride=1,
                 dilation=1,
                 groups=1):
        super(SpectralConv1d, self).__init__()

        self.in_channels = in_channels
        self.out_channels = out_channels
        self.kernel_size = kernel_size
        self.stride = stride
        self.dilation = dilation
        self.groups = groups
        pad = dilation * (kernel_size - 1) // 2

        layer = nn.Conv1d(in_channels, out_channels, kernel_size=kernel_size,
                          stride=stride, padding=pad, dilation=dilation, groups=groups)
        # todo test if weights change with this call
        nn.init.orthogonal_(layer.weight)
        self.layer = spectral_norm(layer)

    def forward(self, inputs):
        return self.layer(inputs)


class SpectralNormalizedLinear(nn.Module):

    "Linear for spectral normalisation and orthogonal initialisation"

    def __init__(self,
                 in_features,
                 out_features,
                 bias=True):
        super(SpectralNormalizedLinear, self).__init__()

        self.in_features = in_features
        self.out_features = out_features

        layer = nn.Linear(in_features, out_features, bias)
        nn.init.orthogonal_(layer.weight)
        self.layer = spectral_norm(layer)

    def forward(self, inputs):
        return self.layer(inputs)


"""
class ToTimestepLayer(nn.Module):
    # https://discuss.pytorch.org/t/how-to-multiply-a-weight-matrix-2d-with-a-tensor-3d-but-channel-is-different-in-different-input/26571
    # https://discuss.pytorch.org/t/torch-round-gradient/28628/4
    def __init__(self,
                 fidelity,
                 round_values=False):
        super(ToTimestepLayer, self).__init__()
        self.round_values = round_values
        self.fidelity = fidelity

    def forward(self, tensor):
        res = tensor*self.fidelity
        if self.round_values:
            return torch.round(res)
        else:
            return res
"""


class ToMelSpectrogramLayer(nn.Module):
    """
    Computes mel-spectrograms for the given waveforms.
        Args:
    apply_mu_decode: whether to apply mu-law inversion to the input waveforms.
    In EATS both the real data and generator outputs are mu-law'ed, so this is
    always set to True.
    max_jitter_steps: maximum number of steps by which the input waveforms are
    randomly jittered if jitter = True (ignored otherwise).
    """

    def __init__(self, max_jitter_steps=60):
        super(ToMelSpectrogramLayer, self).__init__()
        self.max_jitter_steps = max_jitter_steps
        self.mu_decode = MuLawDecoding()
        self.random_crop = RandomCrop(48000, max_jitter_steps)
        self.toMelSpectrogram = MelSpectrogram(
            sample_rate=24000, n_fft=2048, f_min=80., f_max=7600., n_mels=80)

    def forward(self, x, jitter, apply_mu_decode):
        """
        Args:
            x (the wave): waveforms: a tf.Tensor corresponding to a batch of waveforms sampled at 24 kHz.
            (dtype=torch.float32, shape=[N, sequence_length])
            jitter (bool): whether to apply jitter or not
        """
        if jitter:
            # we only add jitter to the ground truth waves, not to the gen output
            # adding jitter makes autograd = false, since we rebuild the tensor!
            # like torchvision RandomCrop, but works for non-images (1D)
            x = self.random_crop(x)
        # invert_mu_law
        if apply_mu_decode:
            x = self.mu_decode(x)
        # get mel spectrogram:
        x = self.toMelSpectrogram(waveform=x)  # (channel, n_mels, time)
        x = torch.log(1. + 10000.*x)
        return x


###############################
# BIG GAN DEEP classes+modules (currently unused)
###############################
# Spectral normalization base class
class SN(object):
    def __init__(self, num_svs, num_itrs, num_outputs, transpose=False, eps=1e-12):
        # Number of power iterations per step
        self.num_itrs = num_itrs
        # Number of singular values
        self.num_svs = num_svs
        # Transposed?
        self.transpose = transpose
        # Epsilon value for avoiding divide-by-0
        self.eps = eps
        # Register a singular vector for each sv
        for i in range(self.num_svs):
            self.register_buffer('u%d' % i, torch.randn(1, num_outputs))
            self.register_buffer('sv%d' % i, torch.ones(1))

    # Singular vectors (u side)
    @property
    def u(self):
        return [getattr(self, 'u%d' % i) for i in range(self.num_svs)]

    # Singular values;
    # note that these buffers are just for logging and are not used in training.
    @property
    def sv(self):
        return [getattr(self, 'sv%d' % i) for i in range(self.num_svs)]

    # Compute the spectrally-normalized weight
    def W_(self):
        W_mat = self.weight.view(self.weight.size(0), -1)
        if self.transpose:
            W_mat = W_mat.t()
        # Apply num_itrs power iterations
        for _ in range(self.num_itrs):
            svs, us, vs = power_iteration(
                W_mat, self.u, update=self.training, eps=self.eps)
        # Update the svs
        if self.training:
            with torch.no_grad():  # Make sure to do this in a no_grad() context or you'll get memory leaks!
                for i, sv in enumerate(svs):
                    self.sv[i][:] = sv
        return self.weight / svs[0]


class SNConv2d(nn.Conv2d, SN):
        # 2D Conv layer with spectral norm for biggan deep
    def __init__(self, in_channels, out_channels, kernel_size, stride=1,
                 padding=0, dilation=1, groups=1, bias=True,
                 num_svs=1, num_itrs=1, eps=1e-12):
        nn.Conv2d.__init__(self, in_channels, out_channels, kernel_size, stride,
                           padding, dilation, groups, bias)
        SN.__init__(self, num_svs, num_itrs, out_channels, eps=eps)

    def forward(self, x):
        return F.conv2d(x, self.W_(), self.bias, self.stride,
                        self.padding, self.dilation, self.groups)


class SNLinear(nn.Linear, SN):
    # Linear layer with spectral norm for biggan deep
    def __init__(self, in_features, out_features, bias=True,
                 num_svs=1, num_itrs=1, eps=1e-12):
        nn.Linear.__init__(self, in_features, out_features, bias)
        SN.__init__(self, num_svs, num_itrs, out_features, eps=eps)

    def forward(self, x):
        return F.linear(x, self.W_(), self.bias)


# Embedding layer with spectral norm for biggan deep
# We use num_embeddings as the dim instead of embedding_dim here
# for convenience sake
class SNEmbedding(nn.Embedding, SN):
    def __init__(self, num_embeddings, embedding_dim, padding_idx=None,
                 max_norm=None, norm_type=2, scale_grad_by_freq=False,
                 sparse=False, _weight=None,
                 num_svs=1, num_itrs=1, eps=1e-12):
        nn.Embedding.__init__(self, num_embeddings, embedding_dim, padding_idx,
                              max_norm, norm_type, scale_grad_by_freq,
                              sparse, _weight)
        SN.__init__(self, num_svs, num_itrs, num_embeddings, eps=eps)

    def forward(self, x):
        return F.embedding(x, self.W_())


class DBlock(nn.Module):
    # Residual block for the spectogram discriminator
    def __init__(self, in_channels, out_channels, which_conv=SNConv2d, wide=True,
                 preactivation=False, activation=None, downsample=None,):
        super(DBlock, self).__init__()
        self.in_channels, self.out_channels = in_channels, out_channels
        # If using wide D (as in SA-GAN and BigGAN), change the channel pattern
        self.hidden_channels = self.out_channels if wide else self.in_channels
        self.which_conv = which_conv
        self.preactivation = preactivation
        self.activation = activation
        self.downsample = downsample

        # Conv layers
        self.conv1 = self.which_conv(self.in_channels, self.hidden_channels)
        self.conv2 = self.which_conv(self.hidden_channels, self.out_channels)
        self.learnable_sc = True if (
            in_channels != out_channels) or downsample else False
        if self.learnable_sc:
            self.conv_sc = self.which_conv(in_channels, out_channels,
                                           kernel_size=1, padding=0)

    def shortcut(self, x):
        if self.preactivation:
            if self.learnable_sc:
                x = self.conv_sc(x)
            if self.downsample:
                x = self.downsample(x)
        else:
            if self.downsample:
                x = self.downsample(x)
            if self.learnable_sc:
                x = self.conv_sc(x)
        return x

    def forward(self, x):
        if self.preactivation:
            # h = self.activation(x) # NOT TODAY SATAN
            # Andy's note: This line *must* be an out-of-place ReLU or it
            #              will negatively affect the shortcut connection.
            h = F.relu(x)
        else:
            h = x
        h = self.conv1(h)
        h = self.conv2(self.activation(h))
        if self.downsample:
            h = self.downsample(h)

        return h + self.shortcut(x)


class Attention(nn.Module):
    # A non-local block as used in SA-GAN
    # Note that the implementation as described in the paper is largely incorrect;
    # refer to the released code for the actual implementation.
    def __init__(self, ch, which_conv=SNConv2d, name='attention'):
        super(Attention, self).__init__()
        # Channel multiplier
        self.ch = ch
        self.which_conv = which_conv
        self.theta = self.which_conv(
            self.ch, self.ch // 8, kernel_size=1, padding=0, bias=False)
        self.phi = self.which_conv(
            self.ch, self.ch // 8, kernel_size=1, padding=0, bias=False)
        self.g = self.which_conv(
            self.ch, self.ch // 2, kernel_size=1, padding=0, bias=False)
        self.o = self.which_conv(
            self.ch // 2, self.ch, kernel_size=1, padding=0, bias=False)
        # Learnable gain parameter
        self.gamma = P(torch.tensor(0.), requires_grad=True)

    def forward(self, x, y=None):
        # Apply convs
        theta = self.theta(x)
        phi = F.max_pool2d(self.phi(x), [2, 2])
        g = F.max_pool2d(self.g(x), [2, 2])
        # Perform reshapes
        theta = theta.view(-1, self. ch // 8, x.shape[2] * x.shape[3])
        phi = phi.view(-1, self. ch // 8, x.shape[2] * x.shape[3] // 4)
        g = g.view(-1, self. ch // 2, x.shape[2] * x.shape[3] // 4)
        # Matmul and softmax to get attention maps
        beta = F.softmax(torch.bmm(theta.transpose(1, 2), phi), -1)
        # Attention map times g path
        o = self.o(torch.bmm(g, beta.transpose(1, 2)).view(-1,
                                                           self.ch // 2, x.shape[2], x.shape[3]))
        return self.gamma * o + x

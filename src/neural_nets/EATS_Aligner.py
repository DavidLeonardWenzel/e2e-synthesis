import torch
import torch.nn as nn
from neural_nets.modules import ConditionalBatchNorm1d, OrthogonalMaskedConv1d, OrthogonalConv1d
import torch.nn.functional as F


class Aligner(nn.Module):
    def __init__(self,
                 device,
                 sample=False,
                 token_vocab_size=133,
                 noise_channel=128,
                 sigma2=10.,
                 ground_truth_alignment=False,
                 ):
        super(Aligner, self).__init__()
        self.device = device
        self.token_vocab_size = token_vocab_size
        self.noise_channel = noise_channel
        self.sigma2 = sigma2
        self.ground_truth_alignment = ground_truth_alignment
        self.sample = sample
        if self.sample:
            self.in_sequence_length = 600
            self.out_sequence_length = 6000  # 30 seconds at 200Hz, then decoded/upsampled
        else:
            self.in_sequence_length = 400  # sentence phones padded to 400 characters
            self.out_sequence_length = 400  # 2 secs at 200Hz, later decoded/upsampled

        embedding_dim = 256

        # embedding:
        self.phone_embed_layer = nn.Embedding(
            num_embeddings=token_vocab_size, embedding_dim=embedding_dim, padding_idx=0)  # will out N, 256, 400
        # preprocessing layer:
        self.pre_process_conv = OrthogonalConv1d(
            in_channels=embedding_dim, out_channels=embedding_dim, kernel_size=3)
        # main, 60 dilated convolutions:
        self.dilated_con_blocks = nn.ModuleList()
        for _ in range(10):
            # 10 blocks * 6 convs per block = 60 convolutions total.
            for a, b in [(1, 2), (4, 8), (16, 32)]:
                self.dilated_con_blocks.append(
                    DilatedConBlock(in_channels=256, out_channels=256, dilation_a=a, dilation_b=b))

        # predict token length from these:
        self.conditionalBatchNorm1d = ConditionalBatchNorm1d(
            num_features=256)
        self.convolution_layer_1 = OrthogonalConv1d(
            in_channels=embedding_dim, out_channels=embedding_dim, kernel_size=1)
        self.convolution_layer_2 = OrthogonalConv1d(
            in_channels=embedding_dim, out_channels=1, kernel_size=1)

        # select via softmax which weight (=most likely correct timestep amount per token) to choose:
        self.softmax_layer = nn.Softmax(dim=2)  # along second axis

        # after aligning/duplicating  phones with batch multiplication, one more post process layer is added:
        self.post_process = ConditionalBatchNorm1d(
            num_features=256)  # Relu included!

    def forward(self, tokens, lengths, noise, out_offset, ground_truth_token_lengths):
        """
            tokens (int, shape=[N], in_sequence_length=400/600): batch of token sequences indicating the ID of each token,
                                    padded to a fixed maximum sequence length (400 for training, 600 for
                                    sampling). Tokens may either correspond to raw characters or phonemes (as
                                    output by Phonemizer)
            lengths (int, shape=[N]): indicates the true length <= in_sequence_length of each
                                      sequence in token_sequences before padding was added.
            noise (float, shape=[N, 128]):      128D noise sampled from a standard isotropic Gaussian (N(0,1)).
            out_offset (int, shape=[N]): first timestep to output. Randomly sampled for training, 0 for sampling

        """
        #######################################################
        # embed tokens:
        #######################################################
        tokens = self.phone_embed_layer(tokens)

        ######################################################
        # create mask for convolutions, indicating valid entries of token_sequences
        ######################################################
        # 400/600 for training/sampling
        mask = torch.tensor(list(range(self.in_sequence_length)), device=self.device)[
            None, :] < lengths[:, None]
        # N, 400, 1, broadcast dimension
        conv_mask = mask[:, :, None].transpose(1, 2).to(self.device)

        ################################################################
        # feed phones and noise through 10 dilated conv layers:
        #################################################################
        # https://pytorch.org/docs/stable/generated/torch.nn.Conv1d.html
        # https://pytorch.org/docs/stable/generated/torch.nn.BatchNorm1d.html
        # currently, our inputs are N-L-C (32/400/258), meaning N=(batch), L=(length of each seq.), C=(channels/embedding dims)
        # batchnorm and conv expects it to be N-C-L, thus we transpose before!
        tokens = self.pre_process_conv(tokens.transpose(1, 2))
        # note that the first convolution is not found in the pseudo-code on p.15, but in the image on p.3

        for dilated_con_block in self.dilated_con_blocks:
            tokens = dilated_con_block(
                tokens, noise, conv_mask)
        unaligned_features = tokens.clone()

        #################################################################
        # predict token length!
        #################################################################
        tokens = self.conditionalBatchNorm1d(tokens, noise)  # w. leaky relu
        tokens = self.convolution_layer_1(
            tokens)  # w. kernel size 1, out 256
        tokens = self.conditionalBatchNorm1d(tokens, noise)  # w. lekay relu
        tokens = self.convolution_layer_2(
            tokens)  # w. kernel s. 1, out 1

        token_lengths = F.relu(tokens.transpose(1, 2)[
            :, :, 0])  # -> [N, 600]

        predicted_token_lengths = token_lengths * mask

        # the predicted token lengths are small floats (and look like milliseconds), but should be interpreted as timesteps in 200hz
        if self.ground_truth_alignment:
            token_lengths = ground_truth_token_lengths  # overwrite!

        #################################################################
        # calculate token centres and total length for length loss:
        #################################################################
        token_ends = torch.cumsum(
            token_lengths, dim=-1)  # [Batch, AmountTokens]

        token_centres = token_ends - \
            (0.5 * token_lengths)  # [Batch, AmountTokens]

        # Compute predicted length as the last valid entry of token_ends. -> [N]
        # this is how long the whole utterance should be if the durations are correct!
        # for utterance loss, its actually more of a sequence length then token length

        # for 'ground truth per phone loss', apply mask:

        # sum(predicted_token_lengths, dim=1) is equal to the above predicted_sequence_length!)
        # (and hopefully close to sum of ground truth length eventually)
        #################################################################
        # align tokens to predicted token length
        #################################################################
        # Compute output grid:
        # out_offset and out sequence length is given in 200 hz already, same as token lenghts and therefore t. centres

        # [N, out_sequence_length=400]
        out_pos = (torch.tensor(list(range(self.out_sequence_length)), device=self.device)[
            None, :] + out_offset[:, None])[:, :, None]  # -> [N, 400, 1]
        # out pos are now the timesteps starting from the randomly sampled n, and then 2 seconds, so 400 timesteps from there
        # for e.g. timesteps 7000-7400, if 7000 is the first timestep to output
        diff = token_centres[:, None, :] - out_pos
        """
        diff is not easy to interpret:
         -> [N, 6000,600 for sampling]
         -> [N, 400, 400 for training]
         batch | timestep | token-centre-to-timstep--distance
         along each timestep to output, we substract that timestep from each token centre
         thus, per timestep per phone, the value closest to zero is the "correct" phone
         this value shifts along the different timesteps (+1 each time),
         so that later phones are closer to zero at later timesteps
         example:
         diff[0][0] with ground truth values: (first batch, first timestep, differences to all token centres:)
        [[...] -50.031,    -29.251,    -11.667,     -2.076,     13.909,] [...]
        -> for the first timestep, diff[0][0][63] (index 63) is closest to zero with -2.076
        -> out_offset[0] -> 1138.617, meaning the audio was cut at timestep 1138
        -> the ground truth token_centre at that index (token_centre[0][63]) is:
            tensor([1136.541])
        -> meaning token #63 has it's true centre at timestep 1136.541, thus being closest!
        -> the ones before and after are further away from the random offset 1138:
        token_centres[0][62:65]
            tensor([1126.949, 1136.541, 1152.526])
        -> in other words, for timestep 0, token-centre #63 is closest to 0 when we substract 1138 steps from all t.centres
        -> thus, it is the correct first phone to choose., i.e. the starting phone given the random audio cut!
        now, looking at the second timesteps difference, we get:
        diff[0][1][60:68]:
        ([-51.031, -30.251, -12.667,  -3.076,  12.909,  32.091,  57.667,  75.251 [...])
        we can see that the diff values are all one higher (because timestep to predict is one higher)
        but the same index (64) has the least difference, now with -3.076, 1 further away from 0
        but still this timestep should have the same phone
        after a few timesteps, we get to the point where the next index, 65, is closer to 0:
        diff[0][6][60:68]:
        tensor([-56.031, -35.251, -17.667,  -8.076, 7.909,  27.091,  52.667,  70.251])
        starting with timestep 6, the centre-difference of the phone with index 65 is closest to 0.
        now we know, for the 2 second random window, phone 64 is needed 5 times, then phone 65 and so on!

        next, in order to be able to use argmax/softmax to efficiently find the value closest to zero,
        we make sure that the value closest to 0 is the highest num (by making all other numbers high negative numbers)
        it will look like this:
        torch.argmax(masked_logits[0][1]) = 63
        [...]
        torch.argmax(masked_logits[0][6]) = 64

        what are masked logits?

        first, we can get logits by squaring all numbers and take the negative:
        -(diff**2)
        thus, -(diff**2)[0][0][60:68] is:
        ([ -2503.150,   -855.612,   -136.124,     -4.310,   - \
         193.458,  -1095.019,  -3441.845,  -5814.193])
        and torch.argmax(-(diff**2)[0][0])
            -> tensor(63)
        at timestep 6, its, 64, and lastly, at timestep 399, it is 88. phones 63-88 is exactly the 2 sec window we need, in phone indexes.
        However, there are three problems remaining:
        1. ) with high timesteps (for eg in long utterances or higher frequencies)
        the squared values can explode, thus we scale them down, doing:
        -(diff**2 / 10) instead. the scaling factor is sigma2 and also called temperature parameter.

        2. ) indices where we zero padded might have gotten a length prediction that is randomly closer to 0,
        especially at training start. we can help by making sure indices where we padded 0s are very far away from being 0.
        note that we cannot apply a regular mask, 0ing those values, because we look for values close to 0!
        instead, the token-centre of zero padded values are right now always the t.centre of the last phone
        so, we want them to be further away from 0 then the "true" phones
        thus, we create an "inverse mask", with high values which we will substract from the squared scaled values:
        inverse_mask = 1e9 * (~mask[:, None, :])
        "~" inverses the mask vector, which indicated True True True [...] False False False tokens
        when inverted, the multiplication results in all values that were "False" to be very high!
        i.e, 0, 0, 0, [...], 1000000000, 1000000000

        inverse_mask.shape
            torch.Size([2, 1, 400]) (N, a single value (0 or 1000000000), for each of the 400 phones)

        3. ) we don't actually want argmax, but allow the phones to blend in with each other at the border between 2 phones

        all in all: masked_logits = -(diff**2 / self.sigma2) - 1e9 * (~mask[:, None, :])
        now, padded values will definitely not be winning argmax/softmax.
        
        In the papers words:
        To compute a[t] (feature at timestep), 
        we obtain interpolation weights for the token representations h[n] using a softmax over
        the squared distance between t and c[n] , scaled by a temperature parameter sigma² = 10
        """
        masked_logits = -(diff**2 / self.sigma2) - \
            1e9 * (~mask[:, None, :])
        # weights are softmax along the second axis (weights=self.softmax_layer(masked_logits)),
        # which is the axis containing the scaled phone length difference
        # weights.shape: torch.Size([2, 400, 400]) N, timesteps, phones
        # torch.argmax(weights[0][0])
        # tensor(63)
        # torch.argmax(weights[0][6])
        # tensor(64)
        # torch.argmax(weights[0][399])
        # tensor(88)
        # all correct!
        # these weights are lastly multiplied with unaligned_features.transpose(1, 2).shape
        # torch.Size([2, 400, 256]) # N, phones, features

        #aligned_features = torch.einsum('noi,nid->nod', self.softmax_layer(masked_logits), unaligned_features.transpose(1, 2))
        # equivalent to:
        tokens = torch.bmm(self.softmax_layer(masked_logits),
                           unaligned_features.transpose(1, 2))

        """proof of phones blending in:
        torch.argmax(weigths[0][0]) = 63
        unaligned_phones[0][63]
        1.275,  2.081, -2.054,  0.053,  0.544, -1.552,  2.383,  3.461, -5.587,
        and tokens / aligned phones[0][0] is:
        1.275,  2.081, -2.054,  0.053,  0.544, -1.552,  2.383,  3.461, -5.587,
        -> softmax weight for both indexes is 1, thus no blend, but:
        torch.argmax(weigths[0][399])
        tensor(88)
        original tensor 88:
        1.660, -3.338,  0.324, -2.685,  0.520,  0.172, -1.874,  0.995,  0.164,
        tokens[0][399]
        1.630, -3.329, 0.308,  -2.634,  0.578,  0.106
        
        The difference is because the actual softmax weights are:
        weights[0][399][88:90]
        tensor([    0.990,     0.010])   
        sometimes, they blend even more, i.e
        weights[0][6][62:66]
            tensor([    0.000,     0.434,     0.566,     0.000])
        """
        ########################################################
        # final layer: linear noise activation, then batch norm with noise condition and ReLU activation
        ########################################################
        tokens = self.post_process(tokens.transpose(1, 2), noise)

        # the predicted sequence+token lengths are used for a length loss!
        return tokens, predicted_token_lengths


class DilatedConBlock(nn.Module):
    """f is a stack of dilated convolutions (van den Oord et al., 2016) interspersed
    with batch normalisation (Ioffe & Szegedy, 2015) and ReLU activations.
    The latents z modulate the scale and shift parameters of the batch normalisation layers (Dumoulin
    et al., 2017; De Vries et al., 2017).

    in channels / out channels for convolution sizes,
    a and b for dilation factors
    """

    def __init__(self,
                 in_channels,
                 out_channels,
                 dilation_a,
                 dilation_b):
        super(DilatedConBlock, self).__init__()

        self.in_channels = in_channels

        self.batch_norm_layer = ConditionalBatchNorm1d(in_channels)

        self.dilated_conv_a = OrthogonalMaskedConv1d(in_channels, out_channels,
                                                     kernel_size=3, dilation=dilation_a)

        self.dilated_conv_b = OrthogonalMaskedConv1d(in_channels=in_channels,
                                                     out_channels=out_channels,
                                                     dilation=dilation_b,
                                                     kernel_size=3
                                                     )

    def forward(self, tokens, noise, conv_mask):
        block_inputs = tokens.clone()
        # with dilation a:
        tokens = self.batch_norm_layer(tokens, noise)
        tokens = self.dilated_conv_a(tokens, conv_mask)

        # with dilation b:
        tokens = self.batch_norm_layer(tokens, noise)
        tokens = self.dilated_conv_b(tokens, conv_mask)

        tokens += block_inputs  # stack it!
        return tokens

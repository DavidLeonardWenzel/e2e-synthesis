import torch.nn as nn
import torch
from torch.nn.utils import spectral_norm
from neural_nets.modules import SpectralConv1d
from neural_nets.modules import SpectralConditionalBatchNorm1d
from utils.misc import mu_law_encoding


class GAN_TTS_Generator(nn.Module):
    def __init__(self,
                 in_channels,
                 z_channels,
                 sample):
        super(GAN_TTS_Generator, self).__init__()

        self.in_channels = in_channels
        self.z_channels = z_channels
        self.sample = sample

        self.preprocess = SpectralConv1d(in_channels, 768, kernel_size=3)
        self.gblocks = nn.ModuleList([
            GBlock(768, 768, z_channels, 1),
            GBlock(768, 768, z_channels, 1),
            GBlock(768, 384, z_channels, 2),
            GBlock(384, 384, z_channels, 2),
            GBlock(384, 384, z_channels, 2),
            GBlock(384, 192, z_channels, 3),
            GBlock(192, 96, z_channels, 5)
        ])
        self.postprocess = nn.Sequential(
            SpectralConv1d(96, 1, kernel_size=3),
            nn.Tanh()
        )

        #self.mu_encode = MuLawEncoding()

    def forward(self, inputs, z):
        inputs = self.preprocess(inputs)
        outputs = inputs
        for (_, layer) in enumerate(self.gblocks):
            outputs = layer(outputs, z)
        outputs = self.postprocess(outputs)

        # mulaw encode the output wave for training, not for sampling:
        if not self.sample:
            outputs = mu_law_encoding(outputs, quantization_channels=256)
        return outputs


class GBlock(nn.Module):
    def __init__(self,
                 in_channels,
                 hidden_channels,
                 z_channels,
                 upsample_factor):
        super(GBlock, self).__init__()

        self.in_channels = in_channels
        self.hidden_channels = hidden_channels
        self.z_channels = z_channels
        self.upsample_factor = upsample_factor

        self.condition_batchnorm1 = SpectralConditionalBatchNorm1d(
            in_channels, z_channels)
        self.relu_upsample_conv_block = nn.Sequential(
            nn.ReLU(inplace=False),
            UpsampleNet(in_channels, in_channels, upsample_factor),
            SpectralConv1d(in_channels, hidden_channels, kernel_size=3)
        )

        self.condition_batchnorm2 = SpectralConditionalBatchNorm1d(
            hidden_channels, z_channels)
        self.relu_conv_block = nn.Sequential(
            nn.ReLU(inplace=False),
            SpectralConv1d(hidden_channels, hidden_channels,
                           kernel_size=3, dilation=2)
        )

        self.residual1_upsample_conv_block = nn.Sequential(
            UpsampleNet(in_channels, in_channels, upsample_factor),
            SpectralConv1d(in_channels, hidden_channels, kernel_size=1)
        )

        self.condition_batchnorm3 = SpectralConditionalBatchNorm1d(
            hidden_channels, z_channels)
        self.third_stack_relu_conv_block = nn.Sequential(
            nn.ReLU(inplace=False),
            SpectralConv1d(hidden_channels, hidden_channels,
                           kernel_size=3, dilation=4)
        )

        self.condition_batchnorm4 = SpectralConditionalBatchNorm1d(
            hidden_channels, z_channels)
        self.fourth_stack_relu_conv_block = nn.Sequential(
            nn.ReLU(inplace=False),
            SpectralConv1d(hidden_channels, hidden_channels,
                           kernel_size=3, dilation=8)
        )

    def forward(self, inputs, z):
        outputs = self.condition_batchnorm1(inputs, z)
        outputs = self.relu_upsample_conv_block(outputs)
        outputs = self.condition_batchnorm2(outputs, z)
        outputs = self.relu_conv_block(outputs)

        residual_outputs = self.residual1_upsample_conv_block(inputs) + outputs

        outputs = self.condition_batchnorm3(residual_outputs, z)
        outputs = self.third_stack_relu_conv_block(outputs)
        outputs = self.condition_batchnorm4(outputs, z)
        outputs = self.fourth_stack_relu_conv_block(outputs)

        outputs = outputs + residual_outputs

        return outputs


class UpsampleNet(nn.Module):
    def __init__(self,
                 input_size,
                 output_size,
                 upsample_factor):

        super(UpsampleNet, self).__init__()
        self.input_size = input_size
        self.output_size = output_size
        self.upsample_factor = upsample_factor

        layer = nn.ConvTranspose1d(input_size, output_size, upsample_factor * 2,
                                   upsample_factor, padding=upsample_factor // 2)
        nn.init.orthogonal_(layer.weight)
        self.layer = spectral_norm(layer)

    def forward(self, inputs):
        outputs = self.layer(inputs)
        outputs = outputs[:, :, : inputs.size(-1) * self.upsample_factor]
        return outputs

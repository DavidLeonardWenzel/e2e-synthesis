An open source implementation of [Google DeepMinds approach to End-to-End Adversarial Text-Speech](https://deepmind.com/research/publications/End-to-End-Adversarial-Text-to-Speech)

![Proposed Generator](./img/architecture.png)

It contains implementations for
- the proposed aligner module
- the GAN-TTS Generator as an upsampler and uRWDs as discriminators, inspired by [Yang Gengs implementation]()
- uses the proposed length loss, hinge embedding loss and DTW as proposed in the paper
- DTW loss is based on [Maghoumis implementation](https://github.com/Maghoumi/pytorch-softdtw-cuda) with an additional warping cost.

This project is created in the context of my thesis at the [IMS](https://www.ims.uni-stuttgart.de/) at the University of Stuttgart. 

The goals of the project are:

- implement a GAN that can be trained on a single speaker dataset and single GPU
- allow for modulations and ablations such as german speakers and direct text input (no phonemizer, exchangable discriminators) 


Note that the master branch includes modifications to the EATS baseline to allow the model to train with low batch sizes. 

# Step-by-step guide: 

## Prepare the code
- download the code :arrow_down::  
    - `git clone https://github.tik.uni-stuttgart.de/DavidLeonardWenzel/e2e-synThesis`

- install the required python modules :woman_technologist: :man_technologist::  
    - `cd e2esynthesis`
- activate your virtualenv (pip env or conda) :computer::
    - `pip install -r requirements.txt`

## Prepare the dataset
- download [LJ Speech](https://keithito.com/LJ-Speech-Dataset/) or a comparable dataset
- put the dataset in the data folder on root level (or change the path accordingly in preprocess.py) 
- the code will resample the dataset to the required 24kHz
- `cd src/`
- run `python preprocess.py`

Note: The preprocessing is done with [this awesome IMS toolkit](https://github.com/Flux9665/PreprocessingForTTS) that needs to be cloned seperately. 

## Train the model 
- run `python train.py`
- optionally, provide on of the following arguments when calling train:
    - discriminator_train_start_steps - *how many steps before adversarial loss is used*
    - discriminator_version - *default is GAN_TTS RWDs, if you pass something else, Parallel WaveGAN disc. is used*
    - train_steps - *how long to train (runs till next checkpoint)*
    - ground_truth_alignment - *whether to use teacher forcing during training (requires preprocessing that provides ground truth phone durations)*
    - eats_length_loss - *whether to use a total utterance length loss (as done in EATS) or use a per-phone-length MSE loss*
    - stft_loss - *whether to replace the BIGGAN spectrogram discriminator with a light-weight STFT spectral and magnitude loss*
    - batch_size - *how big the batch size should be (be aware of GPU memory usage, model is heavy)*
    - log_step - *every x log_step, the model will write a training sample and corresponding ground truth .wav-file, wave+spectrogram visualization and predicted duration values*
    - checkpoint_step - *every x checkpoint_step, the model will save it's learned weights*
    - gpu_id - *whether to use the specified GPU in CUDA or train on CPU (slow) (no parallelization)*